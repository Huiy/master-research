这里是我在研究生期间完成相关项目的所有信息，其中主要包括：

1、软件开发者推荐项目(江苏省省级研究生科研创新项目基金)的论文、专利、软件著作权和相关实现代码；

2、软件维护解决方案项目(基于软件开发者推荐项目进一步深入研究)的相关论文、专利、软件著作权和相关实现代码；

3、面向动态云市场的Agent自适应报价软件(帮助实验室里师兄实现的软件毕业设计)相关源码和软件著作权；

4、带领大学生做大学生科创项目的相关论文、专利和软件著作权(没有放入源码，因为这部分源码不是我写的。)

论文题为：On Automatic Summarization of What and Why Information in Source Code Changes的相关项目为江苏省省级大学生科研创新项目基金，其他为扬州大学校级大学生科创项目。

5、上海复旦大学软件大数据实验室的相关研究（面向软件修改的软件安全数据分析）：

（1）相关研究信息请看:https://bitbucket.org/buganalysis/securitybugfix

（2）相关推荐的系统源码可见bitbucket的开源项目：https://bitbucket.org/Huiy/secdr

6、本人简历：

英文简历	http://files.cnblogs.com/files/huiyang865/Resume.pdf

中文简历	http://files.cnblogs.com/files/huiyang865/%E6%9D%A8%E8%BE%89%E7%9A%84%E7%AE%80%E5%8E%86.pdf